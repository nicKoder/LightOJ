#include <iostream>

using namespace std;

int scarecrows(string s)
{
    int length = s.size();
    int crow_count = 0;

    for(int i = 0; i < length; i++)
    {
        if(s[i] == '.')
        {
            crow_count++;
            i += 2;
        }
    }


    return crow_count;
}

int main()
{
    int test_cases;
    cin >> test_cases;

    for(int i = 1; i <= test_cases; i++)
    {
        int number;
        string s;
        cin >> number;
        cin >> s;

        cout << "Case " << i << ": " << scarecrows(s) << endl;
    }

    return 0;
}
