#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int minimum_k(vector<int> rung_distances)
{
    int total_rungs = rung_distances.size();
    for(int i = 0; i < total_rungs; i++)
        cin >> rung_distances[i];

    for(int i = total_rungs-1 ; i > 0; i--)
        rung_distances[i] -= rung_distances[i-1];


    int max_distance = *max_element(rung_distances.begin(), rung_distances.end());

    return min(max_distance + 1, (int)(max_distance + count(rung_distances.begin(), rung_distances.end(), max_distance) - 1) );
}

int main()
{
    int test_cases;
    cin >> test_cases;

    for(int i = 1; i <= test_cases; i++)
    {
        int n;
        cin >> n;
        vector<int> rungs_distances(n);

        cout << "Case "<< i << ": " << minimum_k(rungs_distances) << endl;
    }

    return 0;
}
