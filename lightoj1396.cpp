#include <iostream>
#include <algorithm>

using namespace std;

bool greater_number(string number1, string number2)
{
    bool b =  false;

    int i = 0;
    int s = number1.size();
    while(i < s)
    {
        if(number1[i] > number2[i])
        {
            b = true;
            break;
        }
        else if(number1[i] < number2[i])
        {
            b = false;
            break;
        }

        i++;
    }

    return b;
}

string increment(string number)
{

    int i = number.size() - 1;
    char *n2 = new char[i+2];
    n2[i+1] = '\0';

    int carry = 1;

    while(i >= 0)
    {
        if(number[i] + carry > '9')
        {
            n2[i] = '0';
            carry = 1;
            i--;
        }
        else
        {
            n2[i] = number[i] + 1;
            i--;
            carry = 0;
            break;
        }
    }

    while(i >= 0)
    {
        n2[i] = number[i];
        i--;
    }

    string result(n2);
    if(carry)
        result.insert(0, "1");

    return result;
}

string next_palindrom(string number)
{
    if(number.size() == 1)
    {
        char temp[2];
        temp[0] = number[0] + 1;
        temp[1] = '\0';
        string s("");

        if(number[0] < '9')
            s.append(temp, 1);
        else
            s.append("11", 2);


        return s;
    }

    string s1 = number.substr(0, number.size()/2);
    string s2 = number.substr((number.size()+1)/2);
    string s3 = s1;
    reverse(s3.begin(), s3.end());
    string next;

    if(number.size() % 2 == 0)
    {
        if(greater_number(s3, s2))
        {
            next =  s1;
            reverse(s1.begin(), s1.end());
            next += s1;
        }
        else
        {
            s1 = increment(s1);
            next =  s1;
            reverse(s1.begin(), s1.end());
            next += s1;
        }
    }
    else
    {
        char mid  = number[number.size()/2];

        if(greater_number(s3, s2))
        {
            next =  s1;
            reverse(s1.begin(), s1.end());
            next += mid;
            next += s1;
        }
        else
        {
            if(mid == '9')
            {
                next = increment(s1);
                s2 = increment(s1);
                reverse(s2.begin(), s2.end());
                mid = '0';
                next += mid;
                next += s2;
            }
            else
            {
                next =  s1;
                reverse(s1.begin(), s1.end());
                mid++;
                next += mid;
                next += s1;
            }
        }
    }
    if(next.size() > number.size())
        next.erase(1, 1);
    return  next;
}


int main()
{
    int test_cases;
    cin >> test_cases;
    for(int i = 1; i <= test_cases; i++)
    {
        string number;
        cin >> number;
        cout << "Case " << i << ": " << next_palindrom(number) << endl;
    }

    return 0;
}
